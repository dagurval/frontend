## Test flipstarter

Before we go live with the first campaign, it's important to do some testing. We should check:

- contributions can be sent.
- contributions can be revoked.
- contributions are updated dynamically.
- the falling floor works.
- campaigns get fullfilled when sufficient funds are committed.

And we should also keep an eye on the user experience and try to find ways to make the process better, the documentation and instructions clearer and so on.
