## WHO WE ARE

We are **EatBCH**. What started with a $5 donation and a promise for sandwiches quickly became the first cryptocurrency-powered charity in Venezuela, and soon bloomed into an international organization working in over 20 communities in two different countries: Venezula and South Sudan both face difficulties due to extreme currency inflation, and access to a global decentralized currency is one of the only ways local people can interact with the global market in a real, simple and fast way. Bitcoin Cash remains the only way on earth to send micro-donations across borders.

We love to help, and we experience great joy from helping each other. Our volunteers often seem just as grateful as the people they are serving.


## WHAT WE DO

Generous patrons around the world donate Bitcoin Cash. We use that money to buy food from local vendors and our local volunteers distribute the meals to those in need. We then document our proof-of-work with time stamped photographs in a transparent and accountable way.


## OUR WORK BY THE NUMBERS

- **100,000** meals delivered in a two-year period
- **20** locations currently serving cooked meals at least once a week.
- **90%** of our funds goes **directly** to food.
- **40+** dedicated volunteers who deliver food to those in the direst need.


## WHERE WE ARE NOW

**Venezuela**: In our almost two-year period, we have helped many communities across the country and have received amazing and generous support from the entire BCH community. Yet, the situation in Venezuela has not improved at all. Many still depend on the meals we give them every week as the only secured meal they have.

**South Sudan**: Thanks to a donor push at the end of 2019, we have been able to expand from $100/month at 5 locations to $200/month at 11 locations. Monthly, we require a budget of around 13 BCH ($2200) to continue operation. Our current locations are as follows:

- Juba - Junub Open Space tech hub
- Juba - Jebel community with Olympian Kenyi Santino
- Juba - Mahad Internally Displaced Person’s (IDP) Camp
- Juba - Maternity and general support at medical centers
- Juba - Lawanci Orphanage
- Juba - School in Mangateen
- Yei - IDPs at the Episcopal Church
- Yei - Single mother’s Vocational Training
- Yei – Maternity support at local medical centers
- Bor - Ataka Tech Hub
- Bor - Blessed Home for Orphans and Delinquents
- Opportunities to expand are unlimited.


## How will our funds be distributed?

**Venezuela**: 7 BCH will help us to continue to provide 4,000 meals each month, plus food bags delivered to vulnerable people/families.

**South Sudan**: 13 BCH will help us to continue to provide 6,000 meals each month, plus food bags delivered to vulnerable people/families.


## How we measure results

Since the very moment we started our work, we have used an accountability system to show that we are using funds as effectively as possible. We take pictures of beneficiaries that also include a time stamp with the hash and block height at that moment. We are a team built on our trust networks, and we also work hard to be accountable through verification. This is our 'proof-of-work'.

Coupled with this, as the biggest cryptocurrency-powered charity in both Venezuela and South Sudan, we use the benefit of the blockchain to provide a level of transparency almost never seen before. All of our donations and our spending are recorded, immutably and permanently, in a public ledger for anybody to see.
